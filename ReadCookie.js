jq(document).ready(function() {

    var required_cookie = "login_response";

    function getCookieVal() {

        var msg = JSON.parse(decodeURIComponent(Cookies.get('login_response')));


        // var decodedCookie = decodeURIComponent(document.cookie);
        // var ca = decodedCookie.split(';');
        // for (var i = 0; i < ca.length; i++) {
        //     if (ca[i].indexOf(required_cookie) > 0)
        //         return JSON.parse(ca[i].split("=")[1]);
        // }
        return msg;
    }

    function setValues() {
        try {
            var message = getCookieVal();
            console.log("Login response " + message);
            var name = message.user_info.name;
            var client_id = message.client_id;
            var user_id = message.user_info.user_id;
        } catch (Exception) {
            window.alert("Login process failed. Please try again.");
            clearCookie();
            window.location.replace("https://login.altor.tech");
        }

        jq(".user_id").eq(0).html(user_id);
        jq(".client_id").eq(0).html(client_id);
        jq(".user_name_text").eq(0).html(name);
    }

    setValues();

    function clearCookie() {
        Cookies.remove('login_response', { domain: 'altor.tech' });
        Cookies.set('client_id', 'None', { domain: 'altor.tech' });
    }

    function logOut() {
        console.log("Logging out.");
        Cookies.remove('login_response', { domain: 'altor.tech' });
        // Cookies.remove('client_id', { domain: 'altor.tech' });
        Cookies.set('client_id', 'None', { domain: 'altor.tech' });
        window.location.replace("https://login.altor.tech");

    }
    //logout function

    jq(".logout_bar").click(function() {
        logOut();


    })

})