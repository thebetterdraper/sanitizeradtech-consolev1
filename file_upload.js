jq(document).ready(function() {

    // var is_thumbnail_uploaded = 0;


    //set css
    jq(".ff_fileupload_wrap").eq(0).css({
        "width": "60%"
    })


    //set scroll
    jq(".upload_bar").eq(0).click(function() {
        scrollToUploader();

    });

    function scrollToUploader() {
        jq("body,html").animate({
                scrollTop: jq(".upload_container").eq(0).offset().top
            },
            800);

    }



    // jq(".upload_videos_button").eq(0).click(function() {

    //     loadBarShow2();

    // })


    jq("#file_upload_section2").change(function() {
        fileUploader();

        jq(".upload_videos_button").eq(0).attr("disabled", true);
        jq(".upload_videos_button").eq(0).css({
            "background-color": "#e4e4e4",
            "border": "none"
        });
        jq(".upload_videos_button").eq(0).html("Finding Media");

    })


    function loadBarShow2() {
        loadBar.mainColor = '#2db1da';
        loadBar.stripColor = '#ff9c5b';
        loadBar.barSpeed = 5;
        loadBar.barHeight = 7;
        loadBar.trigger('show');
        console.log("Trigger.")





    }

    function loadBarHide2() {
        loadBar.trigger('hide');

    }

    jq('#file_upload_section2').bind('change', function() {
        console.log(jq(this));
        var arr = jq(this).val().split("\\");
        setName(arr[arr.length - 1]);
    })

    function setName(name) {
        jq(".filename_text").eq(0).html(name);

    }

    function removeName() {
        jq(".filename_text").eq(0).html("");

    }

    async function fileUploader() {
        var filename = jq('#file_upload_section2')[0].files[0].name;
        filename = filename.replace(/\s+/g, '');
        filename = truncator(filename);

        console.log("FILENAME" + filename);
        // var presigned_url = "https://dgdfmb20rg.execute-api.ap-south-1.amazonaws.com/production/debug/presigned_url/upload/";
        var presigned_url = "https://sqgoarnm8k.execute-api.ap-south-1.amazonaws.com/dev/debug/presigned_url/upload/";
        var fields = {
            client_id: jq(".client_id").eq(0).html(),
            file_id: filename,
            expiry: 100000
        }

        await getURLToUpload(presigned_url, fields);




        // do something
    }


    async function getURLToUpload(presigned_url, fields) {
        await jq.post(presigned_url, fields, function(data, status) {
            if (data.status === "Success") {
                changeFormData(data);
            } else {
                alertDisplay2(data.status);
                removeName();

            }
            console.log(data);
        });


    }

    async function changeFormData(data) {

        var url_base = data.url;
        var fields = data.fields;
        jq("#form_key").attr("value", fields['key']);
        jq("#form_xamzalgo").attr("value", fields['x-amz-algorithm']);
        jq("#form_xamzcred").attr("value", fields['x-amz-credential']);
        jq("#form_xamzdate").attr("value", fields['x-amz-date']);
        jq("#form_xamzsectok").attr("value", fields['x-amz-security-token']);
        jq("#form_policy").attr("value", fields['policy']);
        jq("#form_xamzsig").attr("value", fields['x-amz-signature']);
        jq("form").attr("action", url_base);

        console.log("FIELDS " + data.fields);




        jq(".upload_videos_button").eq(0).attr("disabled", false);
        jq(".upload_videos_button").eq(0).css({
            "background-color": "white",
            "border": "2px solid #ff9c5b",
            "color": "#ff9c5b"
        });
        jq(".upload_videos_button").eq(0).html("Upload Advert");

        // if (is_thumbnail_uploaded == 1)
        //     sendFormData();
    }

    jq(".upload_videos_button").eq(0).click(function() {

        if (jq('#file_upload_section2').val().length > 0) {

            jq(".upload_videos_button").eq(0).attr("disabled", true);
            jq(".upload_videos_button").eq(0).css({
                "background-color": "#e4e4e4",
                "border": "none"
            });
            jq(".upload_videos_button").eq(0).html("Uploading Video");
            sendFormData();
        } else {

            alertDisplay2("No file added yet.")
        }


    })

    function sendFormData() {


        var fd = new FormData(document.getElementsByTagName("form")[0]);
        jq("#myProgress").css("display", "block");

        jq.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                var width_measure

                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        width_measure = percentComplete + "%";
                        percentComplete = parseInt(percentComplete * 100);
                        width_measure = percentComplete + "%";
                        console.log(percentComplete);


                        jq("#myBar").css("width", width_measure);
                        jq("#myBar").html(width_measure);

                        if (percentComplete === 100) {
                            jq("#myProgress").css("display", "none");
                            //setTimeout(refresher, 4000);
                            setName("File has been uploaded.")
                            setTimeout(removeName, 4000);
                            jq(".upload_videos_button").eq(0).html("Upload Advert");

                        }

                    }
                }, false);

                return xhr;
            },
            url: jq("form").attr("action"),
            type: "POST",
            data: fd,
            processData: false,
            contentType: false,
            success: function(result) {

                jq("#file_upload_section2").val("");

                jq("#thumb_img").css("display", "none"); //hide thumbnail
                //make video upload button active again
                jq(".upload_videos_button").eq(0).attr("disabled", false);
                jq(".upload_videos_button").eq(0).css({
                    "background-color": "white",
                    "border": "2px solid #ff9c5b",
                    "color": "#ff9c5b"
                });
                jq(".upload_videos_button").eq(0).html("Upload Advert");


            }
        });


    }

    function refresher() {
        location.reload()

    }

    function uploadThumbnail() {

        jq('#file_upload_section2').val(jq("#thumb_img").attr("src"));

    }


    //alert displaying code
    function alertDisplay2(msg1) {
        notif({

            // success, error, warning, info
            type: "info",

            // Default size
            width: 400,
            height: 60,

            // Default position
            position: "center", // left, center, right, bottom

            // Default autohide
            autohide: true,

            // Default msg
            msg: msg1,

            // Default opacity (Only Chrome, Firefox and Safari)
            opacity: 0.85,

            multiline: 0,
            fade: 0,
            bgcolor: "#59a6bd",
            color: "white",
            timeout: 5000,

            // The z-index of the notification
            zindex: null,

            // The offset in pixels from the edge of the screen
            offset: 0,

            // Callback
            callback: null,

            clickable: false,

            clickable: false,
            animation: 'slide'

        });

    }



    //truncate names

    function truncator(name) {
        name = name.substring(0, 9) + ".." + name.substring((name.length - 3), name.length);
        return name;
    }





})