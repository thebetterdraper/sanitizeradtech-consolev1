jq(document).ready(function() {
    console.log("Chart plotter");

    function padDigits(num) {
        if (num.toString().length != 2)
            num = "0" + num;

        return num;
    }
    //create current timestamp
    function getTimestamp() {
        var currentdate = new Date();
        var datetime = currentdate.getFullYear() + "-" + padDigits(currentdate.getMonth()) + "-" + padDigits(currentdate.getDate());

        var yeardets = padDigits(currentdate.getHours()) + ":" +
            padDigits(currentdate.getMinutes()) + ":" +
            padDigits(currentdate.getSeconds());

        return datetime + " " + yeardets;
    }

    async function getCount() {
        var url = "https://sqgoarnm8k.execute-api.ap-south-1.amazonaws.com/dev/debug/analytics/count/get/";
        var c_id = jq(".client_id").eq(0).html();
        var timestamp_start = getTimestamp();
        var timestamp_end = "";
        var parameters = {
            client_id: c_id,
            timestamp_start: timestamp_start

        }
        var res;
        await jq.post(url, parameters, function(data, status) {
            console.log(JSON.stringify(data));
            res = data;


        });
        return res;
    }

    //getCount();

    function createChartArray(arr) {
        var chartParams = {};
        var ts;
        var count;
        for (var i = 0; i < arr.length; i++) {
            tmp = arr[i].timestamp.substring(5, 10);
            count = arr[i].count;
            chartParams[tmp] = count;

        }
        console.log(JSON.stringify(chartParams));
        return chartParams;
    }
    async function createChart() {
        var res = await getCount();
        if (res.status === "Success") {
            const myOptions = {

                // Title options
                title: '', // chart title
                titleSize: '', // font size
                titleColor: '', // hex color or color name

                // Width/Height
                width: '75%',
                height: '300px',

                // x/y axis names
                xAxisName: 'Date',
                yAxisName: 'Uses',

                // the position of the labels
                // "top" (default), "center", or "bottom"
                valueLabelPosition: 'center',

                // label color
                valueLabelColor: '',

                // bar color
                // also accepts color array for stacked bar chart
                barColor: '#82a1c1',

                // space between bars
                barSpacing: '10px',

                // shows ticks
                showTicks: false,

                // the absolute positioning of the legend
                legendPosition: { top: "0", left: "30px", display: "none" }

            }
            var cParams = createChartArray(res.result);
            drawBarChart(cParams, myOptions, jq("#weekly_chart"));

        } else {
            jq("#weekly_chart").html("No data found.");
        }



    }

    async function initiate() {
        await createChart();
    }

    setTimeout(initiate, 3000);


})