// var jq = $.noConflict();
jq(document).ready(function() {

    //create Queue
    var global_library_list;
    var client_id;
    var user_id;

    jq.LoadingOverlay("show"); //start loading screen

    async function publisherFunc() {
        var list = jq(".arrange_list").eq(0);
        var payload;
        var topic = client_id + "/video/queue/";
        var arr = [];
        var i = 0;
        console.log(list);
        list.find("font").each(function() {
            console.log(jq(this).html());
            arr[i] = jq(this).html();
            i++;
        });
        var res_urls = await getPreSignedURLs(arr);
        if (res_urls != 0) {
            payload = createQueue(res_urls.presigned_urls);
        }

        publish(payload, topic);

        console.log(arr);
        return arr;

    }

    function createQueue(arr) {
        var payload = [];

        for (var i = 0; i < arr.length; i++) {
            payload[i] = {
                id: arr[i].key,
                url: arr[i].url
                    // duration: "10.8"
            }

        }
        console.log(payload);

        return JSON.stringify(payload);

    }

    jq("#publisher").mouse2touch();

    jq("#publisher").click(function() {
        console.log("Here.");
        loadBarShow();
        // alertDisplay("Whoo");
        publisherFunc();



    })

    //play queue

    function playQueue() {
        var topic = client_id + "/video/play/";
        publish("", topic);


    }

    jq(".play_bar").eq(0).click(function() {

        try {
            loadBarShow();
            playQueue();
        } catch (e) {
            alertDisplay("Something went wrong. Try again");
            loadBarHide();

        }

    });

    //stop play

    function stopQueue() {
        var topic = client_id + "/video/stop/";
        removePlayIcon();
        publish("", topic);


    }

    jq(".stop_bar").eq(0).click(function() {
        try {
            loadBarShow();
            stopQueue();
        } catch (e) {
            alertDisplay("Something went wrong. Try again");
            loadBarHide();

        }

    });

    //get state

    function simulateQueueButtonClick() {
        jq(".queue_bar").eq(0).click();

    }


    jq(".queue_bar").eq(0).click(function() {
        try {
            loadBarShow();

            getQueue();
            //scrollToPresentQueue();
        } catch (e) {
            alertDisplay("Something went wrong. Try again");
            loadBarHide();

        }

    });

    function getQueue() {
        var topic = client_id + "/get/state/";
        publish("", topic);


    }


    function viewQueue(arr) {

        jq(".queue_viewer").css({
            "display": "block"
        });
        jq(".queue_viewer").children(".Subheading").html("This is the queue that was uploaded last.");
        var list_contents = "";
        var image_link;

        for (var i = 0; i < arr.length; i++) {
            for (var j = 0; j < global_library_list.length; j++) {
                if (arr[i].id === global_library_list[j].file_id) {

                    image_link = global_library_list[j].thumbnail;

                }
            }
            list_contents += elementGenerator(image_link, arr[i].id, arr[i].duration);
        }


        jq(".queue_list").eq(0).html(list_contents);

    }

    //generate element


    function elementGenerator(img_source, title, duration) {


        var tab = [
            '<li class="thumb_image">',
            '<div class="vidtab">',
            '<div class="vid_navbar">',
            '<div class="del_button"></div>',
            '<div class="play_icon" ><i class="fa fa-play-circle" aria-hidden="true"></i></div>',
            '<div class="download_icon" ><i class="fa fa-download" aria-hidden="true"></i></div>',
            '</div>',
            '<img src="' + img_source + '" alt="" class="vid_img">',
            '<div class="description">',
            '<font class="description_text">' + title + '</font>',
            '<div class="duration_text">' + duration + '</font>',
            '</div>',
            '</div>',
            '</li>'
        ]

        var tab_html = tab.join('');

        return tab_html;




    }

    //delete present live queue

    jq(".delete_present_queue").eq(0).click(function() {
        jq(".queue_viewer").css({
            "display": "none"
        });
    })

    //scroll to live queue

    function scrollToPresentQueue() {
        console.log("Scrolled");
        jq("body,html").animate({
                scrollTop: jq(".upload_container").eq(0).offset().top
            },
            800);

    }









    //loading indicator handler
    function loadBarShow() {
        loadBar.mainColor = '#2db1da';
        loadBar.stripColor = '#ff9c5b';
        loadBar.barSpeed = 5;
        loadBar.barHeight = 7;
        loadBar.trigger('show');
        console.log("Trigger.")





    }

    function loadBarHide() {
        loadBar.trigger('hide');

    }

    //alert handler

    function alertDisplay(msg1) {
        notif({

            // success, error, warning, info
            type: "info",

            // Default size
            width: 400,
            height: 60,

            // Default position
            position: "center", // left, center, right, bottom

            // Default autohide
            autohide: true,

            // Default msg
            msg: msg1,

            // Default opacity (Only Chrome, Firefox and Safari)
            opacity: 0.85,

            multiline: 0,
            fade: 0,
            bgcolor: "#59a6bd",
            color: "white",
            timeout: 5000,

            // The z-index of the notification
            zindex: null,

            // The offset in pixels from the edge of the screen
            offset: 0,

            // Callback
            callback: null,

            clickable: false,

            clickable: false,
            animation: 'slide'

        });

    }











    // Create a client instance
    var client;

    function startConnection() {

        client_id = jq(".client_id").eq(0).html();
        user_id = jq(".user_id").eq(0).html()
        console.log(client_id);


        client = new Paho.MQTT.Client("altor.xyz", Number(8083), user_id);

        client.onConnectionLost = onConnectionLost;
        client.onMessageArrived = onMessageArrived;

        // connect the client
        client.connect({
            onSuccess: proceedTransaction,
            useSSL: true
        });
    }

    setTimeout(function() {
        console.log(jq(".user_id").eq(0).html())
        startConnection();
    }, 3000);

    // set callback handlers



    function proceedTransaction() {
        simulateQueueButtonClick();


        jq.LoadingOverlay("hide");

        console.log("Connected");
        console.log("Client connected");
        //should subscribe to all callbacks here
        alertDisplay("Connected.");
        connectedIndicatorToggle(0);
        var subsrciption_callback = client_id + "/+/+/callback/";
        client.subscribe(subsrciption_callback);
    }


    function publish(payload, topic) {
        var inp = payload;

        message = new Paho.MQTT.Message(inp);
        message.destinationName = topic;
        client.send(message);



    }

    //removes playing icon when playback stopped

    function removePlayIcon() {
        jq(".queue_list").eq(0).children("li").each(function() {
            jq(this).find('.play_icon').css({
                "color": "white"
            });

        })



    }

    //sets next item as playing when FinishedPlayingX is received

    function showAsPlayingFinished(vid_name) {
        console.log(vid_name + " is vid_name");
        jq(".queue_list").eq(0).children("li").each(function(index) {
            var next_elem = ((index + 1) == jq(".queue_list").eq(0).children("li").length) ? 0 : (index + 1);

            if (jq(this).find(".description_text").html() === vid_name) {
                console.log(next_elem + " is what's starting to play videowise");
                console.log("Editing CSS");
                jq(".queue_list").eq(0).children("li").eq(next_elem).find('.play_icon').css({
                    "color": "#f99a5d"
                });

                jq(this).find('.play_icon').css({
                    "color": "white"
                });


            }

        })

    }





    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        connectedIndicatorToggle(1);
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
            alertDisplay("Connection lost. Please try again.")
        }
    }

    // called when a message arrives
    function onMessageArrived(message) {





        console.log("onMessageArrived:" + message.payloadString);

        var msg = message.payloadString;
        //call function to indicate present playing video

        if (msg.indexOf("Finished") >= 0) {
            showAsPlayingFinished(msg.split(" ")[1]);
        }
        //set first element as visibly playing as soon as Playback is announced
        if (msg === "Started Queue Playback") {
            console.log("here2");
            jq(".queue_list").eq(0).children("li").eq(0).find('.play_icon').css({
                "color": "#f99a5d"
            });
        }

        if (msg.indexOf("now_playing") > 0) {
            var message_in_json = JSON.parse(message.payloadString);
            if (message_in_json.queue == null) {
                jq(".queue_viewer").css({
                    "display": "block"
                });
                jq(".queue_list").eq(0).html("");
                jq(".queue_viewer").children(".Subheading").html("No queues in memory.");

            } else {
                viewQueue(message_in_json.queue);
            }
        } else {

            if (((msg.indexOf("Finished")) == -1))
                alertDisplay(msg);
        }

        loadBarHide();





        //can handle according to response
    }



    async function getPreSignedURLs(arr) {
        console.log("HERE");

        var url_to_hit = "https://sqgoarnm8k.execute-api.ap-south-1.amazonaws.com/dev/debug/presigned_url/download/";
        var fields = {
            client_id: jq(".client_id").eq(0).html(),
            files: JSON.stringify(arr),
            expiry: 5000

        }
        var response;
        await jq.post(url_to_hit, fields, function(data, status) {
            if (data.status === "Success") {
                console.log(data + " is data");
                response = data;

            } else {
                alertDisplay("Please try again.");
                response = 0;
            }
        });

        return response;



    }


    //retrieve library

    jq("#get_library").click(function() {
        loadBarShow();
        getLibrary();
    })

    async function retrieveLibrary() {
        var url = "https://sqgoarnm8k.execute-api.ap-south-1.amazonaws.com/dev/debug/sanitizer/get/";
        var total_library = "";
        await jq.post(url, {
            client_id: jq(".client_id").eq(0).html(),
        }, function(data, status) {
            if (data.status === "Success") {
                global_library_list = data.items;
                for (var i = 0; i < data.items.length; i++) {
                    var img_source = data.items[i].thumbnail;
                    var name = data.items[i].file_id;
                    var duration = "10.8";
                    total_library += elementGenerator(img_source, name, duration);
                    loadBarHide();


                }
            } else {
                alertDisplay("Something went wrong. Try again.");
                loadBarHide();
            }
        });
        //console.log(total_library);
        jq(".thumbnails").html(total_library);

    }

    async function getLibrary() {
        await retrieveLibrary();
    }

    setTimeout(getLibrary, 2000);

    //truncate names

    function truncator(name) {
        name = name.substring(0, 9) + ".." + name.substring((name.length - 3), name.length);
        return name;
    }

    function connectedIndicatorToggle(val) {
        if (val == 0) {
            jq(".user_online_symbol").eq(0).css({
                "background-color": "#6aff9c"
            })
        } else {
            jq(".user_online_symbol").eq(0).css({
                "background-color": "#ff6a6a"
            })

        }

    }



})