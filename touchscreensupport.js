jq(function() {
    jq.extend(jq.support, { touch: "ontouchend" in document });
    var f = null,
        e = !1,
        k = null,
        g = !1,
        h = null,
        m = null,
        l = !1,
        n = function() { e = !1 },
        p = function() { g && (window.clearTimeout(m), g = !1, h = null) },
        q = function(c) {
            g || (g = !0, h = c.changedTouches[0], m = window.setTimeout(function() {
                g = !1;
                var a = document.createEvent("MouseEvent"),
                    b = h;
                a.initMouseEvent("mouseup", !0, !0, window, 1, b.screenX, b.screenY, b.clientX, b.clientY, !1, !1, !1, !1, 0, null);
                b.target.dispatchEvent(a);
                a = document.createEvent("MouseEvent");
                a.initMouseEvent("mousedown", !0, !0, window, 1, b.screenX, b.screenY, b.clientX, b.clientY, !1, !1, !1, !1, 2, null);
                b.target.dispatchEvent(a);
                a = document.createEvent("MouseEvent");
                a.initMouseEvent("contextmenu", !0, !0, window, 1, b.screenX + 50, b.screenY + 5, b.clientX + 50, b.clientY + 5, !1, !1, !1, !1, 2, null);
                b.target.dispatchEvent(a);
                l = !0;
                h = null
            }, 800))
        },
        r = function(c) {
            var a = "";
            if (!(1 < c.touches.length)) {
                switch (c.type) {
                    case "touchstart":
                        if (jq(c.changedTouches[0].target).is("select")) return;
                        var a = c.changedTouches[0],
                            b = "mouseover",
                            d = document.createEvent("MouseEvent");
                        d.initMouseEvent(b, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null);
                        a.target.dispatchEvent(d);
                        b = "mousedown";
                        d = document.createEvent("MouseEvent");
                        d.initMouseEvent(b, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null);
                        a.target.dispatchEvent(d);
                        e ? (window.clearTimeout(k), a.target == f ? (f = null, e = !1, b = "click", d = document.createEvent("MouseEvent"), d.initMouseEvent(b, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null), a.target.dispatchEvent(d),
                            b = "dblclick", d = document.createEvent("MouseEvent"), d.initMouseEvent(b, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null), a.target.dispatchEvent(d)) : (f = a.target, e = !0, k = window.setTimeout(n, 600), q(c))) : (f = a.target, e = !0, k = window.setTimeout(n, 600), q(c));
                        c.preventDefault();
                        return !1;
                    case "touchmove":
                        p();
                        a = "mousemove";
                        c.preventDefault();
                        break;
                    case "touchend":
                        if (l) return l = !1, c.preventDefault(), !1;
                        p();
                        a = "mouseup";
                        break;
                    default:
                        return
                }
                c = c.changedTouches[0];
                b = document.createEvent("MouseEvent");
                b.initMouseEvent(a, !0, !0, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, !1, !1, !1, !1, 0, null);
                c.target.dispatchEvent(b);
                "mouseup" == a && e && c.target == f && (b = document.createEvent("MouseEvent"), b.initMouseEvent("click", !0, !0, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, !1, !1, !1, !1, 0, null), c.target.dispatchEvent(b))
            }
        };
    jq.fn.mouse2touch = function() { jq.support.touch && this.each(function(c, a) { for (var b = ["touchstart", "touchmove", "touchend", "touchcancel"], d = 0; b[d]; d++) a.addEventListener(b[d], r, !1) }); return this }
});